require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

app.use(express.json());
app.use(enableCORS);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde API TechU"});
  }
)

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.get('/apitechu/v2/account/:id', accountController.getAccountV2);
app.get('/apitechu/v2/movement/:id', accountController.getMovement);

app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

app.post('/apitechu/v1/login', authController.loginUserV1);
app.post('/apitechu/v1/logout/:id', authController.logoutUserV1);
app.post('/apitechu/v2/login', authController.loginUserV2);
app.post('/apitechu/v2/logout/:id', authController.logoutUserV2);
app.post('/apitechu/v2/abmov/:id', accountController.addMovement);

    // console.log("for normal");
    // for (var i=0;i<users.length;i++) {
    //    console.log("i=" + i);
    //    if (users[i].id == req.params.id) {
    //    users.splice(i, 1);
    //    deleted = true;
    //    break;
    //   }
    // }

    // console.log("for in");
    // for (var i in users) {
    // for (arrayId in users) {
    //    console.log("comparar " + users[arrayId].id + " y " + req.params.id);
    //    if (users[arrayId].id == req.params.id) {
    //    users.splice(arrayId, 1);
    //    deleted=true;
    //    break;
    //    }
    // }

    // console.log("forEach");
    // users.forEach (function (usuario, index) {
    //    console.log("comparar " + users.id + " y " + req.params.id);
    //    if (user.id == req.params.id) {
    //    users.splice(index, 1);
    //    deleted = true;
    //    console.log("Registro borrado nº " + i);
    //    }
    //  }
    //);

    // console.log("for of");
    // var index = 0;
    // for (user of users) {
    //   console.log("comparar " + user.id + " y " + req.params.id);
    //   if (user.id == req.params.id) {
    //     users.splice(user.id, 1);
    //     console.log("Registro borrado nº " + user.id);
    //     deleted = false;
    //     break;
    //   }
    // index++;
    // }
    //}

    // console.log("Array findIndex");
    // var indexOfElement = users.findIndex(
    //   function(element){
    //     console.log("comparando " + element.id + " y " +   req.params.id);
    //     return element.id == req.params.id
    //   }
    // )
    // console.log("indexOfElement es " + indexOfElement);
    // if (indexOfElement > 0) {
    //   users.splice(indexOfElement, 1);
    //   deleted = true;
    // }
//}

//app.post("/apitechu/v1/monstruo",
//  function (req, res) {
//    console.log("Paramétros");
//    console.log(req.params);
//    console.log("Query String");
//    console.log(req.query);
//    console.log("Headers");
//    console.log(req.headers);
//    console.log("Body");
//    console.log(req.body);
//   }
// )

app.post("/apitechu/v1/monstruo/:p1/:p2",
 function (req, res) {
   console.log("post monstruo");
   console.log(req.params);
   console.log("Query String");
   console.log(req.query);
   console.log("Headers");
   console.log(req.headers);
   console.log("Body");
   console.log(req.body);
 }
)

const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmr10ed/collections/";
// const mLabAPIKey = "apiKey=sOxwPLEanLpVvud9cdTYyklORGjC1Y9R";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function getAccountById(req, res) {
  console.log("GET /apitechu/v2/account/:id");
  var id = req.params.id;  // Puede ser userid
  var query='q={"idUsuario":' + id + '}';
  console.log("La consulta es " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  //console.log("Client created");
  //console.log("user?q={"""id"":req.params.id"}&" + mLabAPIKey)
  //httpClient.get("user?q={""id"":"req.params.id"}&" + mLabAPIKey,
  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body)  {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;  //var response = body;
        } else {
          var response = {
            "msg" : "usuario no encontrado"
          }
          res.status(404);
        }
      }
      //  var response = !err ? body[0] : {
      //    "msg" : "Error obteniendo usuarios"
      //  }
      res.send(response);
    }
  )
}


module.exports.getAccountById   = getAccountById;

const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmr10ed/collections/";
// const mLabAPIKey = "apiKey=sOxwPLEanLpVvud9cdTYyklORGjC1Y9R";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');
function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");
  var result = {};
  var users = require('../usersauth.json');
  // var users = require('../usuarios.json');

  if (req.query.$count=="true") {
  //  console.log("COUNT=" + req.query.$count);
    result.count=users.length;
    console.log("users.length=" + users.length);
  }

  result.users=req.query.$top ? users.slice(0,req.query.$top) : users;
  //console.log("top users=" + result.users);

  res.send(result);
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("user?c=true" + mLabAPIKey,
    function(err, resMLab, body)  {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )
}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");
  var id = req.params.id;
  var query='q={"id":' + id + '}';
  console.log("La consulta es " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  //console.log("Client created");
  //console.log("user?q={"""id"":req.params.id"}&" + mLabAPIKey)
  //httpClient.get("user?q={""id"":"req.params.id"}&" + mLabAPIKey,
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body)  {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "usuario no encontrado"
          }
          res.status(404);
        }
      }
      //  var response = !err ? body[0] : {
      //    "msg" : "Error obteniendo usuarios"
      //  }
      res.send(response);
    }
  )

}

function createUserV1(req, res) {
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

var newUser = {
"first_name": req.body.first_name,
"last_name": req.body.last_name,
"email": req.body.email,
}
var users = require('../usuarios.json');
users.push(newUser);
io.writeUserDatatoFile(users);
console.log("Usuario añadido con éxito");
res.send({"msg" : "Usuario añadido con éxito"})
}

function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");
  var query = 'c=true';
  console.log("query es " + query);
  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (body.length == 0) {
        var response = {
          "mensaje" : "No hay usuarios activos"
        }
        res.send(response);
      } else {
        //console.log("body=" + body);
        var a = body + 1;
        var newUser = {
          "id": a,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          // "password": req.body.password   // sin encriptar
          "password": crypt.hash(req.body.password)
        }
        httpClient.post("user?" + mLabAPIKey, newUser,
          function(err, resMLab, body)  {
            console.log("Usuario creado en Mlab");
            res.status(201).send({"msg" : "Usuario creado"});
          }
        )
      }
    }
  );
}


function deleteUserV1(req, res) {
 var users = require('../usuarios.json');
 var deleted = false;
 console.log("DELETE /apitechu/v1/users/:id");
 console.log("id es " + req.params.id);  var users = require('../usuarios.json');
 for (var [index, user] of users.entries()) {
   console.log("comparando " + user.id + " y " +  req.params.id);
   if (user.id == req.params.id) {
     console.log("La posicion " + index + " coincide");
     users.splice(index, 1);
     deleted = true;
     break;
   }
 }

  if (deleted) {
    io.writeUserDatatoFile(users);
  }
  var msg = deleted ? "Usuario borrado" : "Usuario no encontrado."
  console.log(msg);
  res.send({"msg" : msg});
}

module.exports.getUsersV1   = getUsersV1;
module.exports.getUsersV2   = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;

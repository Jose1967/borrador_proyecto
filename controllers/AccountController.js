const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmr10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountV2(req, res) {
  console.log("GET /apitechu/v2/account/:id");
  var id = req.params.id;
  console.log("req.params=" + req.params);
  console.log('id=' + id);
  var query = 'q={"userid":' + id + '}';
  console.log("Query es " + query);
  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log(body);
      if (err) {
        response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body;
        } else {
          response = {
            "msg" : "El usuario no tiene cuentas"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function getMovement(req, res) {
  console.log("GET /apitechu/v2/movement/:id");
  var id = req.params.id;
  console.log("req.params=" + req.params);
  console.log('id=' + id);
  var query = 'q={"ibanid":' + id + '}&s={"date":-1, "time":-1}';
  console.log("Query es " + query);
  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log(body);
      if (err) {
        response = {
          "msg" : "Error obteniendo movimientos"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body;
        } else {
          response = {
            "msg" : "El usuario no tiene movimientos"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function addMovement(req, res) {
 console.log("POST /apitechu/v2/abmov/:id");
 req.body.entry = parseFloat(req.body.entry);
 //console.log("req.body.entry=" + req.body.entry);
 httpClient = requestJson.createClient(baseMLabURL);
 var newMov = {
   "ibanid": req.body.id,
   "amount": req.body.entry,
   "date": req.body.date,
   "time": req.body.time
 }
 if (!isNaN(req.body.entry)) {
   httpClient.post("movement?" + mLabAPIKey, newMov,
     function(err, resMLab, body)  {
       console.log("Movimiento añadido");
       res.status(201).send({"msg" : "Movimiento añadido"});
     }
   )
 }
 else {
   res.status(201).send({"msg" : "Movimiento no valido"});
   console.log("movimiento no valido");
 }
}

module.exports.getAccountV2 = getAccountV2;
module.exports.getMovement = getMovement;
module.exports.addMovement = addMovement;

const io = require('../io')
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmr10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function loginUserV1(req, res) {
  console.log("LOGIN /apitechu/v1/users");
  var users = require('../usersauth.json');
  var existe = false;
  for (var i=0;i<users.length;i++) {
      //console.log("i=" + i);
      //console.log("user=" + users[i].email + "pass=" + users[i].password);
      if (req.body.email == users[i].email && req.body.password == users[i].password) {
      //console.log("Credenciales correctas, id=" + users[i].email);
      users[i].logged = true;
      existe = true;
      idu = i;
      break;
      } else {
        //console.log("Credenciales incorrectas, id=" + users[i].email);
      }
  }

  var msg = existe ? "Login correcto idUsuario=" + users[idu].id : "Login incorrecto "
  console.log(msg);
  res.send({"msg" : msg});

  io.writeUserDatatoFile(users);
  res.send(req.body.email);
}

function logoutUserV1(req, res) {
  console.log("LOGOUT /apitechu/v1/users");
  var users = require('../usersauth.json');
  var logado = false;
  for (var i=0;i<users.length;i++) {
      //console.log("i=" + i);
      if (req.params.id == users[i].id && users[i].logged == true) {
      //console.log("Usuario logado, id=" + users[i].id);
      msg = "Logout correcto ";
      logado = true;
      delete (users[i].logged);
      break;
      } else {
        //console.log("Usuario no logado, id=" + req.params.id);
        msg = "Logout incorrecto";
      }
   }

  io.writeUserDatatoFile(users);
  var msg = logado ? "Logout correcto idUsuario=" + req.params.id : "Logout incorrecto "
  console.log(msg);
  res.send({"msg" : msg});
}

function deleteUserV1(req, res) {
 var users = require('../usuarios.json');
 var deleted = false;
 console.log("DELETE /apitechu/v1/users/:id");
 console.log("id es " + req.params.id);  var users = require('../usuarios.json');
 for (var [index, user] of users.entries()) {
   console.log("comparando " + user.id + " y " +  req.params.id);
   if (user.id == req.params.id) {
     console.log("La posicion " + index + " coincide");
     users.splice(index, 1);
     deleted = true;
     break;
   }
 }

  if (deleted) {
    io.writeUserDatatoFile(users);
  }
  var msg = deleted ? "Usuario borrado" : "Usuario no encontrado."
  console.log(msg);
  res.send({"msg" : msg});
}

//function loginUserV2(req, res) {
//  console.log("LOGIN /apitechu/v2/users");
//  var query = 'q={"email": "' + req.body.email + '"}';
//  var putBody = '{"$set":{"logged":true}}';
//  var httpClient = requestJson.createClient(baseMLabURL);
//  httpClient.get("user?" + query + "&" + mLabAPIKey,
//  function(err, resMLab, body)  {
//    if (err) {
//      var response = {
//        "msg" : "Error obteniendo usuario"
//      }
//      res.status(500);
//    } else {
//      if (body.length > 0) {
//        var response = body[0];
//        //console.log("req.body.password=" + req.body.password);
//        //console.log("body[0].password=" + body[0].password);
//        if (crypt.checkPassword(req.body.password, body[0].password)) {
          //console.log("put logged");
//          var putBody = '{"$set":{"logged":true}}';
//          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
//          function(err1, resMLab1, body1)  {
//            if (!err1) {
//              console.log("insertado el logged");
//            }
//          }
//        )
//        }
//      } else {
//        var response = {
//          "msg" : "usuario no encontrado"
//        }
//        res.status(404);
//      }
//    }
//    res.send(response);
//    }
//  )
//}

function loginUserV2(req, res) {
 console.log("POST /apitechu/v2/login");
 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);
 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     //
      console.log("body=" + body);
      if (body == '') {
        var response = {
        "msg" : "Login incorrecto, email y/o passsword no encontrados"
        }
        // console.log("No hay body");
        res.status(401);
        res.send(response);

      } else {
        var isPasswordcorrect = crypt.checkPassword(req.body.password, body[0].password);
        console.log("Password correct is " + isPasswordcorrect);
        if (!isPasswordcorrect) {
          var response = {
            "msg" : "Login incorrecto, email y/o passsword no encontrados"
          }
          res.status(401);
          res.send(response);
        } else {
          console.log("Got a user with that email and password, logging in");
          query = 'q={"id" : ' + body[0].id +'}';
          console.log("Query for put is " + query);
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");
              var response = {
                "msg" : "Usuario logado con éxito",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          )
        }

      }

   }
 );
}

function logoutUserV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");
 var query = 'q={"id": ' + req.params.id + '}';
 //console.log("query es " + query);
 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.loginUserV1  = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.loginUserV2  = loginUserV2;
module.exports.logoutUserV2 = logoutUserV2;

const bcrypt = require('bcrypt');
// Returns encripted String.
function hash(data) {
  console.log("Hashsing data");
  return bcrypt.hashSync(data, 10);  // devuelve un string, dato encriptado
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
  console.log("Checking password");
  return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;  // Se exporta la función para ser utilizada fuera
module.exports.checkPassword = checkPassword;

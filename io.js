const fs = require('fs');

function writeUserDatatoFile(data) {
  //console.log("writeUserDatatoFile");
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usersauth.json", jsonUserData, "utf8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Usuario persistido");
      }
    }
  )
}

module.exports.writeUserDatatoFile = writeUserDatatoFile
